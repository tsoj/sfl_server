# SFL Server

The SFL Server is an IntelliJ Plugin (currently only targeting the IDEA IDE) that
provides a generalized interface for software fault localization tools (SFL tools).
SFL tool results can be loaded in a JSON format describing a partial program
transition system, called "fault describing transition system" (FDTS).
These loaded SFL results can be used by extensions to provide services to the
user (programmer) that may be helpful in resolving bugs.

The documentation can be found in [here](./doc/Documentation.md).
