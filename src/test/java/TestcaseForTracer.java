import org.jetbrains.annotations.NotNull;

import static java.lang.Integer.parseInt;

public class TestcaseForTracer {
    public static int main(String @NotNull [] args) {
        if(args.length < 2)
            return 0;
        
        var x = parseInt(args[0]);
        var a = args[1];
        int i = x + a.length();
        
        if(a.length() > 100)
            return 0;

        var c = false;

        for(; i>0; i/=2){
            if(i == 7)
                c = true;
            if(i == 5)
                c = true;
        }

        var maybeBug = false;

        if(c){
            for (var ch : a.toCharArray()){
                if (x == parseInt("" + ch)){
                    x = -1;
                }
            }

            x = x*3;

            maybeBug = x != -3;
        }
        else{
            while (true){
                if(x == 1)
                    return 0;
                if(x == 0){
                    maybeBug = true;
                    break;
                }
                if(x<0)
                    x += 1;
                else
                    x -= 1;
            }
        }

        if(maybeBug)
        {
            var z = x % 2;
            assert z == 0;
        }

        return 0;
    }
}
