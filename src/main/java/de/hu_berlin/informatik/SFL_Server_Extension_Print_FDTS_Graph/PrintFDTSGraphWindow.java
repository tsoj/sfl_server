package de.hu_berlin.informatik.SFL_Server_Extension_Print_FDTS_Graph;

import com.intellij.uiDesigner.core.GridConstraints;
import com.mxgraph.swing.mxGraphComponent;

import javax.swing.*;
import java.util.List;

public class PrintFDTSGraphWindow {
    private JPanel panel1;

    PrintFDTSGraphWindow(List<mxGraphComponent> graphComponents){
        for(var graphComponent : graphComponents)
            panel1.add(graphComponent, new GridConstraints());
    }

    public JComponent getContent() {
        return  panel1;
    }
}
