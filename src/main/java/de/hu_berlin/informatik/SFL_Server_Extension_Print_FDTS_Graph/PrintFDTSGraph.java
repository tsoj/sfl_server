package de.hu_berlin.informatik.SFL_Server_Extension_Print_FDTS_Graph;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.mxgraph.layout.*;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.view.mxGraph;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTSLoader;
import de.hu_berlin.informatik.SFL_Core.Predicates.Utils;
import de.hu_berlin.informatik.SFL_Server.Connectable;
import de.hu_berlin.informatik.SFL_Server.Consumer;
import de.hu_berlin.informatik.SFL_Server_Extension_Print_Json_FDTS.PrintJsonFDTSWindow;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class PrintFDTSGraph implements Consumer {

    private ActiveElementChangeCallback activeElementChangeCallback = null;

    private static class FdtsIdTuple{
        FdtsIdTuple(FDTS fdts, FDTS.ID id){
            this.fdts = fdts;
            this.id = id;
        }
        FDTS fdts;
        FDTS.ID id;
    }

    private final HashMap<Object, FdtsIdTuple> fdtsIdTupleTreeMap = new HashMap<>();

    @Override
    public void run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList) {
        assert !fdtsList.isEmpty();// TODO proper error handling

        var graphComponents = new ArrayList<mxGraphComponent>();
        for(var fdts : fdtsList){
            graphComponents.add(createGraphComponent(fdts));
        }

        var printFDTSGraphWindow = new PrintFDTSGraphWindow(graphComponents);
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        Content content = contentFactory.createContent(printFDTSGraphWindow.getContent(), getName(), false);
        toolWindow.getContentManager().addContent(content);
        content.setCloseable(true);
    }

    @Override
    public String getName() {
        return "Print FDTS Graph";
    }

    private mxGraphComponent createGraphComponent(FDTS fdts){
        mxGraph graph = new mxGraph();
        var parent = graph.getDefaultParent();

        var nodeToObject = new TreeMap<FDTS.NodeID, Object>();

        graph.getModel().beginUpdate();
        try
        {
            for(var nodeID : fdts.getNodeIDs())
            {
                // TODO: better description, fix height width
                var graphNode = graph.insertVertex(
                        parent, null,
                        Utils.getJavaConditionString(fdts.getPredicate(nodeID)),
                        0, 0, 300,40
                );
                nodeToObject.put(nodeID, graphNode);
                fdtsIdTupleTreeMap.put(graphNode, new FdtsIdTuple(fdts, nodeID));
            }
            for(var edgeID : fdts.getEdgeIDs()){
                var graphEdge = graph.insertEdge(
                        parent, null,
                        Utils.getJavaConditionString(fdts.getPredicate(edgeID)),
                        nodeToObject.get(edgeID.getFrom()), nodeToObject.get(edgeID.getTo())
                );
                fdtsIdTupleTreeMap.put(graphEdge, new FdtsIdTuple(fdts, edgeID));
            }
        }
        finally
        {
            graph.getModel().endUpdate();
        }


        graph.getSelectionModel().setSingleSelection(true);
        graph.getSelectionModel().addListener(mxEvent.CHANGE, new mxEventSource.mxIEventListener() {
            @Override
            public void invoke(Object sender, mxEventObject evt) {
                var selected = graph.getSelectionModel().getCell();
                if(activeElementChangeCallback != null && selected != null)
                {
                    var tuple = fdtsIdTupleTreeMap.get(selected);
                    activeElementChangeCallback.onChange(tuple.fdts, tuple.id);
                    System.out.println(tuple.fdts.toString() + tuple.id.toString());
                }
            }
        });
        //graph.addListener(mxEvent.SELECT);



        // TODO: fix issue with loops
        var layout = new mxCompactTreeLayout(graph);
        //layout.set
        layout.setHorizontal(false);

        // center the circle
        //int radius = 100;
        //layout.setX0(300);
        //layout.setY0(300);
        //layout.setRadius(radius);
        //layout.setMoveCircle(true);

        layout.execute(graph.getDefaultParent());

        graph.setCellsBendable(false);
        graph.setCellsDisconnectable(false);
        graph.setCellsCloneable(false);
        graph.setCellsDeletable(false);
        graph.setCellsEditable(false);
        graph.setCellsMovable(false);
        graph.setCellsResizable(false);
        graph.setCellsSelectable(true);
        graph.setCellsLocked(true);

        return new mxGraphComponent(graph);
    }

    @Override
    public void setActiveElement(@NotNull FDTS fdts, @NotNull FDTS.ID id) {
        //TODO
    }

    @Override
    public void setActiveElementCallback(@NotNull ActiveElementChangeCallback callback) {
        activeElementChangeCallback = callback;
    }
}
