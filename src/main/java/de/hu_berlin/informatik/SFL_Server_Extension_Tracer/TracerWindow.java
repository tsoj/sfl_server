package de.hu_berlin.informatik.SFL_Server_Extension_Tracer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TracerWindow {
    private JButton stopButton;
    private JPanel panel1;

    TracerWindow(Runnable stopFunction){
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                stopFunction.run();
            }
        });
    }

    public JComponent getContent() {
        return panel1;
    }
}
