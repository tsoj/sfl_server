package de.hu_berlin.informatik.SFL_Server_Extension_Tracer;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.intellij.util.messages.MessageBusConnection;
import com.intellij.xdebugger.*;
import com.intellij.xdebugger.breakpoints.XLineBreakpoint;
import com.intellij.xdebugger.impl.breakpoints.XBreakpointUtil;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTSLoader;
import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.AtLine;
import de.hu_berlin.informatik.SFL_Core.Predicates.Utils;
import de.hu_berlin.informatik.SFL_Server.Consumer;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Tracer implements Consumer {

    ToolWindow toolWindow;
    Content windowContent;
    MessageBusConnection messageBusConnection;
    ActiveElementChangeCallback activeElementChangeCallback;


    @Override
    public void setActiveElement(@NotNull FDTS fdts, @NotNull FDTS.ID id) {
        // Can't set active id manually
    }

    @Override
    public void setActiveElementCallback(@NotNull ActiveElementChangeCallback callback) {
        activeElementChangeCallback = callback;
    }

    @Override
    public void run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList) {

        assert fdtsList.size() == 1; // TODO: proper error handling
        var fdts = fdtsList.get(0);

        this.toolWindow = toolWindow;
        var tracerWindow = new TracerWindow(this::cleanUp);
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        windowContent = contentFactory.createContent(tracerWindow.getContent(), getName(), false);
        toolWindow.getContentManager().addContent(windowContent);
        windowContent.setCloseable(false);

        messageBusConnection = project.getMessageBus().connect();
        messageBusConnection.subscribe(XDebuggerManager.TOPIC, new XDebuggerManagerListener() {
            @Override
            public void processStarted(@NotNull XDebugProcess debugProcess) {

                var breakpointToNodeMap = addBreakpoints(project, fdts);

                var visited = new ArrayList<FDTS.NodeID>();

                debugProcess.getSession().addSessionListener(new XDebugSessionListener() {
                    @Override
                    public void sessionPaused() {
                        // TODO: check if we are at a relevant break point, and update the current FDTS status. Continue if
                        // conditions aren't met
                        //debugProcess.getSession().resume();
                        var filename = Objects.requireNonNull(
                                debugProcess.getSession().getCurrentPosition()
                        ).getFile().getUrl();
                        var line = debugProcess.getSession().getCurrentPosition().getLine();
                        var nodeID = breakpointToNodeMap.get(filename).get(line);
                        assert nodeID != null;
                        System.out.println("Searching path ...");
                        var path = de.hu_berlin.informatik.SFL_Core.FDTS.Utils.findPath(
                                fdts, nodeID, visited, fdts.getEdgeIDs()
                        );
                        if(path != null) {
                            var l = 0;
                            for (var nid : path) {
                                System.out.println("----------------\n" + l + "\n----------------\n" +
                                        FDTSLoader.getJson(fdts.getPredicate(nid))
                                );
                                l++;
                            }
                        }

                        System.out.println("--------------!!!--------->");
                        System.out.println("-----------???--\n" +
                                FDTSLoader.getJson(fdts.getPredicate(nodeID))
                        );
                        for (var nid : visited) {
                            System.out.println("----------------\n" +
                                    FDTSLoader.getJson(fdts.getPredicate(nid))
                            );
                        }
                        System.out.println("--------------!!!---------<");

                        System.out.println("Stopped searching: " + path);
                        if(path != null) {
                            visited.add(nodeID);
                            activeElementChangeCallback.onChange(fdts, nodeID);
                        }
                        else
                            ApplicationManager.getApplication().invokeLater(() -> debugProcess.getSession().resume());

                    }
                });

                // TODO remove breakpoints that I set
            }
        });
    }
    //TODO debug and try on new examples


    @Override
    public String getName() {
        return "Tracer";
    }

    // TODO: only stop at breakpoints of previous breakpoints align with FDTS

    private static HashMap<String, HashMap<Integer, FDTS.NodeID>> addBreakpoints(@NotNull Project project, FDTS fdts){
        HashMap<String, HashMap<Integer, FDTS.NodeID>> breakpointToNodeMap = new HashMap<>();
        Runnable r = ()-> {

            for(var nodeID : fdts.getNodeIDs()){
                var predicate = fdts.getPredicate(nodeID);
                for (AtLine atLine : Utils.getSubPredicateList(predicate, AtLine.class)) {
                    var virtualFile = LocalFileSystem.getInstance().findFileByPath(atLine.path);
                    assert virtualFile != null;// TODO proper error handling
                    var lineIndex = atLine.line - 1;
                    //TODO: maybe skip empty lines
                    if (XDebuggerUtil.getInstance().canPutBreakpointAt(
                            project,
                            virtualFile,
                            lineIndex
                    )) {
                        var getBreakPoint = new Object(){
                            XLineBreakpoint<?> run() throws ExecutionException, TimeoutException {
                                var breakpointPromise = XBreakpointUtil.toggleLineBreakpoint(
                                        project,
                                        Objects.requireNonNull(XDebuggerUtil.getInstance().createPosition(
                                                virtualFile,
                                                lineIndex
                                        )),
                                        null,
                                        false,
                                        false,
                                        true
                                );
                                return breakpointPromise.blockingGet(10, SECONDS);
                            }
                        };
                        try {

                            var breakpoint = getBreakPoint.run();
                            if(breakpoint == null)// toggled breakpoint off, so now I have to toggle the breakpoint back on
                                breakpoint = getBreakPoint.run();

                            assert breakpoint != null;

                            var conditionString = Utils.getJavaConditionString(predicate);
                            //TODO: use condition only that must be true when being on the mentioned line
                            //TODO: combine conditions of all nodes referring to this line
                            breakpoint.setCondition(conditionString);
                            var filename = breakpoint.getFileUrl();
                            var line = breakpoint.getLine();
                            breakpointToNodeMap.computeIfAbsent(filename, k -> new HashMap<>());
                            breakpointToNodeMap.get(filename).put(line, nodeID);
                            assert breakpointToNodeMap.get(filename).get(line) != null;

                        } catch (TimeoutException | ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };

        WriteCommandAction.runWriteCommandAction(project, r);

        return breakpointToNodeMap;
    }

    void cleanUp(){
        messageBusConnection.dispose();
        toolWindow.getContentManager().removeContent(windowContent, true);
    }
}
