package de.hu_berlin.informatik.SFL_Core.FDTS;

import java.util.ArrayList;
import java.util.List;

/**
 * Utils provides utility function for FDTSs.
 */
public class Utils {
    // TODO use nullable and notnull for all functions

    /**
     * Returns a path from one node to another node if it exists, given additional requirements.
     * @param fdts FDTS
     * @param from node where the path should start
     * @param to node where the path should end
     * @param allowedNodes list of nodes that are allowed to be part of the path. If null, all nodes are allowed.
     * @param allowedEdges list of edges that are allowed to be part of the path. If null, all edges are allowed.
     * @return list of NodeIDs that described a path, or null of there doesn't exist a path from 'from' to 'to'
     */
    public static List<FDTS.NodeID> findPath(
            FDTS fdts,
            FDTS.NodeID from, FDTS.NodeID to,
            List<FDTS.NodeID> allowedNodes,
            List<FDTS.EdgeID> allowedEdges
    ){
        if(from.equals(to)) {
            var result = new ArrayList<FDTS.NodeID>();
            result.add(to);
            return result;
        }

        if(allowedEdges == null)
            allowedEdges = fdts.getEdgeIDs();
        if(allowedNodes == null)
            allowedNodes = fdts.getNodeIDs();

        var newAllowedNodes = new ArrayList<FDTS.NodeID>();
        for(var nodeID : allowedNodes){
            if(!nodeID.equals(from)){
                newAllowedNodes.add(new FDTS.NodeID(nodeID));
            }
        }

        for(var edgeID : fdts.getOutgoingEdges(from)){
            if((allowedEdges.contains(edgeID) && newAllowedNodes.contains(edgeID.to)) || edgeID.to.equals(to)) {
                var result = findPath(fdts, edgeID.to, to, newAllowedNodes, allowedEdges);
                if (result != null)
                    result.add(0, from);
                return result;
            }
        }

        return null;
    }

    /**
     * Returns a path from any starting node to a given node if it exists, given additional requirements.
     * @param fdts FDTS
     * @param to node where the path should end
     * @param allowedNodes list of nodes that are allowed to be part of the path. If null, all nodes are allowed.
     * @param allowedEdges list of edges that are allowed to be part of the path. If null, all edges are allowed.
     * @return list of NodeIDs that described a path, or null of there doesn't exist a path from 'from' to 'to'
     */
    public static List<FDTS.NodeID> findPath(
            FDTS fdts,
            FDTS.NodeID to,
            List<FDTS.NodeID> allowedNodes,
            List<FDTS.EdgeID> allowedEdges
    ){
        for(var from : fdts.getStartNodes()){
            var result = findPath(fdts, from, to, allowedNodes, allowedEdges);
            if(result != null)
                return result;
        }
        return null;
    }
}
