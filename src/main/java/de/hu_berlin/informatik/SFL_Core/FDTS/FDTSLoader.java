package de.hu_berlin.informatik.SFL_Core.FDTS;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;
import de.hu_berlin.informatik.SFL_Core.Predicates.*;
import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.AtLine;
import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.BiggerThan;
import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.Equality;
import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.True;
import de.hu_berlin.informatik.SFL_Core.Predicates.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * FDTSLoader is a helper class that loads FDTS instances from JSON files and creates predicates or FDTS instances in
 * JSON format.
 */

public class FDTSLoader {

    /*private static RuntimeTypeAdapterFactory<Predicate> createRuntimePredicateAdapterFactory(){
        var result = RuntimeTypeAdapterFactory
                .of(Predicate.class, "type")
                .registerSubtype(And.class)
                .registerSubtype(Atomic.class)
                .registerSubtype(Future.class)
                .registerSubtype(Globally.class)
                .registerSubtype(Not.class)
                .registerSubtype(Or.class)
                .registerSubtype(TemporalOperator.class)
                .registerSubtype(True.class)
                .registerSubtype(Equality.class)
                .registerSubtype(AtLine.class)
                .registerSubtype(BiggerThan.class);
        var current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(FDTSLoader.class.getClassLoader());
            var loader = ServiceLoader.load(Predicate.class);
            for(var predicate : loader) {
                result.registerSubtype(predicate.getClass());
            }
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }
        return result;
    }*/

    private static final RuntimeTypeAdapterFactory<Predicate> runtimePredicateAdapterFactory = RuntimeTypeAdapterFactory
            .of(Predicate.class, "type")
            .registerSubtype(And.class)
            .registerSubtype(Atomic.class)
            .registerSubtype(Future.class)
            .registerSubtype(Globally.class)
            .registerSubtype(Not.class)
            .registerSubtype(Or.class)
            .registerSubtype(TemporalOperator.class)
            .registerSubtype(True.class)
            .registerSubtype(Equality.class)
            .registerSubtype(AtLine.class)
            .registerSubtype(BiggerThan.class);
    private static final RuntimeTypeAdapterFactory<FDTS> runtimeFDTSAdapterFactory = RuntimeTypeAdapterFactory
            .of(FDTS.class, "type")
            .registerSubtype(FDTSMeta.class)
            .registerSubtype(FDTSImpl.class)
            .registerSubtype(FDTSDecorator.class);

    /**
     * registerFDTS can be used to allow loading and storing of custom implementations of the FDTS interface.
     * @param fdtsType Class type of the FDTS type
     */
    static public void registerFDTS(Class<? extends FDTS> fdtsType){
        runtimeFDTSAdapterFactory.registerSubtype(fdtsType);
    }

    /**
     * predicateType can be used to allow loading and storing of custom implementations of Predicate.
     * @param predicateType Class type of the Predicate type
     */
    static public void registerPredicate(Class<? extends Predicate> predicateType){
        try {
            runtimePredicateAdapterFactory.registerSubtype(predicateType);
        } catch (IllegalArgumentException ignored){}
    }

    static private Gson getGson(){
        return new GsonBuilder()
                .registerTypeAdapterFactory(runtimePredicateAdapterFactory)
                .registerTypeAdapterFactory(runtimeFDTSAdapterFactory)
                .setPrettyPrinting()
                .create();
    }

    static private boolean checkFDTS(FDTS fdts){
        for(var edgeID : fdts.getEdgeIDs())
            if(!Utils.isTemporal(fdts.getPredicate(edgeID)))
                return false;

        for(var nodeId : fdts.getNodeIDs())
            if(Utils.isTemporal(fdts.getPredicate(nodeId)))
                return false;

        return true;
    }

    /**
     * Load an FDTS instance of a given implementation from a JSON string.
     * @param json json string
     * @param classOfT class of FDTS implementation that should be created
     * @param <T> class of FDTS implementation that should be created
     * @return FDTS instance loaded from json
     */
    static public <T extends FDTS> T loadFromJson(String json, Class<T> classOfT){
        var result = getGson().fromJson(json, classOfT);
        if(!checkFDTS(result))
            throw new RuntimeException("FDTS is not formatted correctly");// TODO more infos about error
        return result;
    }

    /**
     * Loads a list of FDTS instances from a json string.
     * Currently, this can only be done to load instances of FDTSMeta, because it doesn't work for some reason
     * if the FDTS type is unspecified in the function definition.
     * @param json json string, top level object must be an array
     * @return list of FDTS instances loaded from json
     */
    static public List<FDTSMeta> loadMetaArrayFromJson(String json){
        //TODO fix, FDTSMeta -> T extends FDTS
        List<FDTSMeta> result = getGson().fromJson(
                json,
                new TypeToken<ArrayList<FDTSMeta>>(){}.getType()
        );
        for(var fdts : result)
            if(!checkFDTS(fdts))
                throw new RuntimeException("FDTS is not formatted correctly");// TODO more infos about error
        return result;

    }

    /**
     * Returns a JSON-formatted string representing a, an instance of type T.
     * This can be used to get the JSON representation of an object that includes an FDTS or a Predicate.
     * @param a object that we want a JSON string of
     * @param <T> type of the object
     * @return JSON string
     */
    static public <T> String getJson(T a){
        return getGson().toJson(a);
    }
}
