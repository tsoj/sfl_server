package de.hu_berlin.informatik.SFL_Core.FDTS;

import de.hu_berlin.informatik.SFL_Core.Predicates.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of the FDTS interface. Nodes are stored in a continuous array, where NodeID points
 * to the index in of the respective node in the array. Edges are stored as a n*n matrix, where n is the number of nodes.
 * If the edge at index i,j is not null, then there exists an edge from node i to node j with the given predicate.
 */

public class FDTSImpl implements FDTS{

    private final ArrayList<Predicate<LogicType.NonTemporal>> nodes;
    private final ArrayList<ArrayList<Predicate<LogicType.Temporal>>> edges;
    private final ArrayList<NodeID> startNodes;
    private final ArrayList<NodeID> endNodes;

    public FDTSImpl(){
        nodes = new ArrayList<>();
        edges = new ArrayList<>();
        startNodes = new ArrayList<>();
        endNodes = new ArrayList<>();
    }

    @Override
    public void clear() {
        nodes.clear();
        edges.clear();
        startNodes.clear();
        endNodes.clear();
    }

    @Override
    public ArrayList<NodeID> getNodeIDs(){
        var result = new ArrayList<NodeID>();
        for(var i = 0; i<nodes.size(); ++i){
            var node = new NodeID(i);
            if(getPredicate(node) != null)
                result.add(node);
        }
        return result;
    }
    @Override
    public ArrayList<EdgeID> getEdgeIDs(){
        var result = new ArrayList<EdgeID>();
        for(var i = 0; i<edges.size(); ++i){
            var from = new NodeID(i);
            for(var j = 0; j<edges.get(i).size(); ++j){
                var to = new NodeID(j);
                var edge = new EdgeID(from, to);
                if(getPredicate(edge) != null)
                    result.add(edge);
            }
        }
        return result;
    }

    @Override
    public ArrayList<EdgeID> getOutgoingEdges(NodeID from){
        var result = new ArrayList<EdgeID>();
        for(var i = 0; i<edges.get(from.id).size(); ++i){
            if(edges.get(from.id).get(i) != null)
                result.add(new EdgeID(from, new NodeID(i)));
        }
        return result;
    }
    @Override
    public ArrayList<EdgeID> getIncomingEdges(NodeID to){
        var result = new ArrayList<EdgeID>();
        for(var i = 0; i<edges.size(); ++i){
            if(edges.get(i).get(to.id) != null)
                result.add(new EdgeID(new NodeID(i), to));
        }
        return result;
    }

    @Override
    public Predicate<LogicType.NonTemporal> getPredicate(NodeID nodeID){
        return nodes.get(nodeID.id);
    }
    @Override
    public Predicate<LogicType.Temporal> getPredicate(EdgeID edgeID){
        return edges.get(edgeID.from.id).get(edgeID.to.id);
    }

    @Override
    public void setPredicate(NodeID nodeID, Predicate<LogicType.NonTemporal> predicate){
        nodes.set(nodeID.id, predicate);
    }
    @Override
    public void setPredicate(EdgeID edgeID, Predicate<LogicType.Temporal> predicate){
        edges.get(edgeID.from.id).set(edgeID.to.id, predicate);
    }
    @Override
    public NodeID addNode(Predicate<LogicType.NonTemporal> predicate) {
        for (var i = 0; i < nodes.size(); ++i){
            if (nodes.get(i) == null) {
                nodes.set(i, predicate);
                return new NodeID(i);
            }
        }

        edges.add(new ArrayList<>());
        assert(edges.size() == nodes.size() + 1);
        for(var i = 0; i<nodes.size(); ++i)
            edges.get(nodes.size()).add(null);

        nodes.add(predicate);

        for (ArrayList<Predicate<LogicType.Temporal>> targetNodes : edges) {
            targetNodes.add(null);
            assert (targetNodes.size() == nodes.size());
        }

        assert(edges.size() == nodes.size());

        return new NodeID(nodes.size() - 1);
    }

    @Override
    public void removeNode(NodeID nodeID) {
        nodes.set(nodeID.id, null);
        for (var edge : edges) {
            edge.set(nodeID.id, null);
        }
        for(var i = 0; i<edges.get(nodeID.id).size(); ++i){
            edges.get(nodeID.id).set(i, null);
        }
    }

    @Override
    public void removeEdge(EdgeID edgeID) {
        edges.get(edgeID.from.id).set(edgeID.to.id, null);
    }

    @Override
    public ArrayList<NodeID> getStartNodes(){
        return startNodes;
    }

    @Override
    public List<NodeID> getEndNodes() {
        return endNodes;
    }

    @Override
    public void setStartNode(NodeID nodeID) {
        unsetStartingNode(nodeID);
        startNodes.add(nodeID);
    }

    @Override
    public void unsetStartingNode(NodeID nodeID) {
        while(startNodes.remove(nodeID));
    }

    @Override
    public void setEndNode(NodeID nodeID) {
        unsetEndNode(nodeID);
        endNodes.add(nodeID);

    }

    @Override
    public void unsetEndNode(NodeID nodeID) {
        while(endNodes.remove(nodeID));
    }

}
