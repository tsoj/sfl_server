package de.hu_berlin.informatik.SFL_Core.FDTS;

import de.hu_berlin.informatik.SFL_Core.Predicates.LogicType;
import de.hu_berlin.informatik.SFL_Core.Predicates.Predicate;

import java.util.List;

/**
 * Decorator class for FDTS.
 */

public class FDTSDecorator implements FDTS{

    private FDTS fdts;

    public FDTSDecorator(FDTS fdts){
        setFdts(fdts);
    }

    public void setFdts(FDTS fdts) {
        this.fdts = fdts;
    }

    @Override
    public void clear() {
        fdts.clear();
    }

    @Override
    public List<NodeID> getNodeIDs() {
        return fdts.getNodeIDs();
    }

    @Override
    public List<EdgeID> getEdgeIDs() {
        return fdts.getEdgeIDs();
    }

    @Override
    public List<EdgeID> getOutgoingEdges(NodeID nodeID) {
        return fdts.getOutgoingEdges(nodeID);
    }

    @Override
    public List<EdgeID> getIncomingEdges(NodeID nodeID) {
        return fdts.getIncomingEdges(nodeID);
    }

    @Override
    public Predicate<LogicType.NonTemporal> getPredicate(NodeID nodeID) {
        return fdts.getPredicate(nodeID);
    }

    @Override
    public Predicate<LogicType.Temporal> getPredicate(EdgeID edgeID) {
        return fdts.getPredicate(edgeID);
    }

    @Override
    public void setPredicate(NodeID nodeID, Predicate<LogicType.NonTemporal> predicate) {
        fdts.setPredicate(nodeID, predicate);
    }

    @Override
    public void setPredicate(EdgeID edgeID, Predicate<LogicType.Temporal> predicate) {
        fdts.setPredicate(edgeID, predicate);
    }

    @Override
    public NodeID addNode(Predicate<LogicType.NonTemporal> predicate) {
        return fdts.addNode(predicate);
    }

    @Override
    public void removeNode(NodeID nodeID) {
        fdts.remove(nodeID);
    }

    @Override
    public void removeEdge(EdgeID edgeID) {
        fdts.removeEdge(edgeID);
    }

    @Override
    public List<NodeID> getStartNodes() {
        return fdts.getStartNodes();
    }

    @Override
    public List<NodeID> getEndNodes() {
        return fdts.getStartNodes();
    }

    @Override
    public void setStartNode(NodeID nodeID) {
        fdts.setStartNode(nodeID);
    }

    @Override
    public void unsetStartingNode(NodeID nodeID) {
        fdts.unsetStartingNode(nodeID);
    }

    @Override
    public void setEndNode(NodeID nodeID) {
        fdts.setEndNode(nodeID);
    }

    @Override
    public void unsetEndNode(NodeID nodeID) {
        fdts.unsetEndNode(nodeID);
    }
}
