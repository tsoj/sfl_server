package de.hu_berlin.informatik.SFL_Core.FDTS;

import de.hu_berlin.informatik.SFL_Core.Predicates.LogicType;
import de.hu_berlin.informatik.SFL_Core.Predicates.Predicate;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * FDTS is the interface for all possible implementations of fault describing transition systems.
 * It models a directed graph which has logic predicates at nodes and temporal logic predicates
 * at edges. Nodes can optionally be selected to be start or end nodes.
 * An FDTS describes a class of program transition paths that might be connected to specific bugs in the program.
 */
public interface FDTS {

    /**
     * ID is the abstract super class of NodeID and EdgeID.
     */
    abstract class ID{}

    /**
     * A NodeID instance can be used to point to a specific node of an FDTS.
     */
    class NodeID extends ID implements Comparable<NodeID> {
        protected int id;
        public NodeID(){}

        /**
         * Constructor of NodeID, creates a new node based on the internal representation of the node system.
         * @param id is the internal pointer to a node. Precise meaning depends on the implementation of the FDTS.
         */
        protected NodeID(int id){
            this.id = id;
        }

        /**
         * Copy constructor of NodeID, creates a deep copy of a NodeID instance.
         * @param nodeID instance of NodeID that should be copied
         */
        public NodeID(NodeID nodeID){
            this.id = nodeID.id;
        }
        @Override
        public boolean equals(Object obj){
            if (obj == null)
                return false;
            if (!(obj instanceof NodeID))
                return false;
            return ((NodeID)obj).id == this.id;
        }

        @Override
        public int compareTo(FDTS.NodeID nodeID) {
            return Integer.compare(this.id, nodeID.id);
        }
    }
    class EdgeID extends ID implements Comparable<EdgeID> {
        public NodeID getTo() {
            return to;
        }

        public NodeID getFrom() {
            return from;
        }

        protected NodeID from, to;
        public EdgeID(){}

        /**
         * EdgeID Constructor, creates a new EdgeID from a starting node and an end node.
         * @param from starting node of edge
         * @param to end node of edge
         */
        public EdgeID(NodeID from, NodeID to){
            this.from = from;
            this.to = to;
        }
        /**
         * Copy constructor of EdgeID, creates a deep copy of a EdgeID instance.
         * @param edgeID instance of EdgeID that should be copied
         */
        public EdgeID(EdgeID edgeID){
            this.from = new NodeID(edgeID.from);
            this.to = new NodeID(edgeID.to);
        }
        @Override
        public boolean equals(Object obj){
            if (obj == null)
                return false;
            if (!(obj instanceof EdgeID))
                return false;
            return ((EdgeID)obj).from.equals(this.from) && ((EdgeID)obj).to.equals(this.to);
        }

        @Override
        public int compareTo(FDTS.EdgeID edgeID) {
            if(this.from.compareTo(edgeID.from) == 0)
                return Integer.compare(this.to.compareTo(edgeID.to), 0);

            return Integer.compare(this.from.compareTo(edgeID.from), 0);
        }
    }

    /**
     * Clears the FDTS, setting it to an empty FDTS.
     */
    void clear();

    /**
     * Gets a list of NodeIDs of all nodes of the FDTS.
     * @return list of all NodeIDs
     */
    List<NodeID> getNodeIDs();
    /**
     * Gets a list of EdgeID of all nodes of the FDTS.
     * @return list of all EdgeIDs
     */
    List<EdgeID> getEdgeIDs();

    /**
     * Gets a list of IDs of all edges and nodes in the FDTS.
     * @return list of all IDs
     */
    default List<ID> getIDs(){
        var result = new ArrayList<ID>(getNodeIDs());
        result.addAll(getEdgeIDs());
        return result;
    }

    /**
     * Returns all edges that have nodeID as origin.
     * @param nodeID origin node of edges
     * @return list of edges that start from nodeID
     */
    List<EdgeID> getOutgoingEdges(NodeID nodeID);
    /**
     * Returns all edges that have nodeID as endpoint.
     * @param nodeID end node of edges
     * @return list of edges that end at nodeID
     */
    List<EdgeID> getIncomingEdges(NodeID nodeID);

    /**
     * Returns the predicate connected to nodeID.
     * @param nodeID node of which the predicate should be returned
     * @return non-temporal predicate connected to nodeID
     */
    Predicate<LogicType.NonTemporal> getPredicate(NodeID nodeID);
    /**
     * Returns the predicate connected to edgeID.
     * @param edgeID edge of which the predicate should be returned
     * @return temporal predicate connected to edgeID
     */
    Predicate<LogicType.Temporal> getPredicate(EdgeID edgeID);
    /**
     * Returns the predicate connected to either an edgeID or a nodeID.
     * @param id edge or node of which the predicate should be returned
     * @return predicate connected to id
     */
    default Predicate<?> getPredicate(ID id){
        if(id instanceof NodeID)
            return getPredicate((NodeID) id);

        assert(id instanceof EdgeID);
        return getPredicate((EdgeID) id);
    }

    /**
     * Gets a list of all predicates connected to any node or edge in the FDTS.
     * @return list of all predicates
     */
    default List<Predicate<?>> getPredicates(){
        var result = new ArrayList<Predicate<?>>();
        for (var id : getIDs()){
            result.add(getPredicate(id));
        }
        return result;
    }

    /**
     * Sets the predicate of nodeID to the given predicate
     * @param nodeID node to which the predicate should be connected to
     * @param predicate non-temporal predicate that should be connected to nodeID
     */
    void setPredicate(NodeID nodeID, Predicate<LogicType.NonTemporal> predicate);
    /**
     * Sets the predicate of edgeID to the given predicate
     * @param edgeID edge to which the predicate should be connected to
     * @param predicate temporal predicate that should be connected to edgeID
     */
    void setPredicate(EdgeID edgeID, Predicate<LogicType.Temporal> predicate);


    /**
     * Creates new node with the given predicate connected to it.
     * @param predicate non-temporal predicate that should be connected to the new node. Must not be null
     * @return NodeID of the newly created node
     */
    NodeID addNode(Predicate<LogicType.NonTemporal> predicate);

    /**
     * Creates new node with an empty predicate connected to it.
     * @return NodeID of the newly created node
     */
    default NodeID addNode(){
        return addNode(new Predicate<LogicType.NonTemporal>() {});
    }

    /**
     * Creates new edge with an empty predicate connected to it.
     * @param edgeID edgeID of the wanted new edge
     */
    default void addEdge(EdgeID edgeID){
        setPredicate(edgeID, new Predicate<LogicType.Temporal>() {});
    }

    /**
     * Removes the given node.
     * @param nodeID node which should be removed
     */
    void removeNode(NodeID nodeID);
    /**
     * Removes the given edge.
     * @param edgeID edge which should be removed
     */
    void removeEdge(EdgeID edgeID);
    /**
     * Removes the given element.
     * @param id edge or node which should be removed
     */
    default void remove(ID id){
        if(id instanceof NodeID)
            removeNode((NodeID) id);
        else{
            assert(id instanceof EdgeID);
            removeEdge((EdgeID) id);
        }
    }

    /**
     * Returns a list of all start nodes of the FDTS.
     * @return list of start nodes
     */
    List<NodeID> getStartNodes();
    /**
     * Returns a list of all end nodes of the FDTS.
     * @return list of end nodes
     */
    List<NodeID> getEndNodes();

    /**
     * Sets nodeID to be a starting node
     * @param nodeID node that should be set up to be a starting node
     */
    void setStartNode(NodeID nodeID);
    /**
     * If nodeID is a starting node, this will set nodeID to not be a starting node.
     * @param nodeID node that should not be a starting node
     */
    void unsetStartingNode(NodeID nodeID);
    /**
     * Sets nodeID to be an end node
     * @param nodeID node that should be set up to be an end node
     */
    void setEndNode(NodeID nodeID);
    /**
     * If nodeID is an end node, this will set nodeID to not be an end node.
     * @param nodeID node that should not be an end node
     */
    void unsetEndNode(NodeID nodeID);

    default boolean isStartNode(NodeID nodeID){
        for(var n : getStartNodes())
            if(n.equals(nodeID))
                return true;
        return false;
    }

    default boolean isEndNode(NodeID nodeID){
        for(var n : getEndNodes())
            if(n.equals(nodeID))
                return true;
        return false;
    }


    /**
     * Sets up the FDTS instance to be semantically deep copy of an instance of any FDTS implementation.
     * @param fdtsFrom fdts that this FDTS instance should be transformed into
     * @param <T> type of fdtsFrom, must implement FDTS
     */
    default <T extends FDTS> void transformFrom(T fdtsFrom){
        this.clear();
        var nodeIsomorphism = new TreeMap<NodeID, NodeID>();
        for(var nodeID : fdtsFrom.getNodeIDs()){
            nodeIsomorphism.put(nodeID, this.addNode(fdtsFrom.getPredicate(nodeID)));
        }
        for(var edgeID : fdtsFrom.getEdgeIDs()){
            this.setPredicate(new FDTS.EdgeID(
                            nodeIsomorphism.get(edgeID.from),
                            nodeIsomorphism.get(edgeID.to)
                    ), fdtsFrom.getPredicate(edgeID)
            );
        }
        for(var nodeID : fdtsFrom.getStartNodes()){
            this.setStartNode(nodeIsomorphism.get(nodeID));
        }
    }
}
