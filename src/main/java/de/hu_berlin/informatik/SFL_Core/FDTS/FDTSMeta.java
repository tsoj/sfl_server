package de.hu_berlin.informatik.SFL_Core.FDTS;

import java.util.Hashtable;

/**
 * FDTSMeta is a decorator class of FDTS that adds a dictionary that can be used to store
 * additional information about a particular FDTS, e.g., filename, suspicousness score, used test cases, etc.
 * This is the default assumption under which FDTS are loaded from files.
 */

public class FDTSMeta extends FDTSDecorator{
    public Hashtable<String, String> info;

    public FDTSMeta(FDTS fdts) {
        super(fdts);
        info = new Hashtable<>();
    }
}
