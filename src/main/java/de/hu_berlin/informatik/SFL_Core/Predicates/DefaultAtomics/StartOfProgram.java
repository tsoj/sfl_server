package de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics;

import de.hu_berlin.informatik.SFL_Core.Predicates.Atomic;

/**
 * StartOfProgram is an Atomic that can be used to require that the program is in the first state of execution.
 */
public class StartOfProgram implements Atomic {
}
