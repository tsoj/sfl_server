package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * BinaryOperator is an abstract class that can be used as a template for predicates that have two sub-predicates.
 * @param <T> either LogicType.NonTemporal when the sub-predicates are non-temporal,
 *  *           or LogicType.Temporal for when the sub-predicates are temporal.
 * @param <T2> either LogicType.NonTemporal when the BinaryOperator is used in a non-temporal context,
 *  *           or LogicType.Temporal for when the BinaryOperator is used in a temporal context.
 */
public abstract class BinaryOperator<T extends LogicType, T2 extends LogicType> implements Predicate<T2>{
    /**
     * Constructor of BinaryOperator.
     * @param x first sub-predicate
     * @param y second sub-predicate
     */
    BinaryOperator(Predicate<T> x, Predicate<T> y){
        subPredicateA = x;
        subPredicateB = y;
    }

    /**
     * Returns the first sub-predicate.
     * @return the first sub-predicate
     */
    public Predicate<T> getSubPredicateA(){
        return subPredicateA;
    }
    /**
     * Returns the second sub-predicate.
     * @return the second sub-predicate
     */
    public Predicate<T> getSubPredicateB(){
        return subPredicateB;
    }

    /**
     * Sets the first sub-predicate to predicate.
     * @param predicate new predicate that the first sub-predicate should be set to
     */
    public void setSubPredicateA(Predicate<T> predicate){
        subPredicateA = predicate;
    }
    /**
     * Sets the second sub-predicate to predicate.
     * @param predicate new predicate that the second sub-predicate should be set to
     */
    public void setSubPredicateB(Predicate<T> predicate){
        subPredicateB = predicate;
    }

    private Predicate<T> subPredicateA;
    private Predicate<T> subPredicateB;
}
