package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * UnaryOperator is an abstract class that can be used as template for predicates that are unary operators.
 * @param <T> either LogicType.NonTemporal when the sub-predicates are non-temporal,
 *  *           or LogicType.Temporal for when the sub-predicates are temporal.
 * @param <T2> either LogicType.NonTemporal when the UnaryOperator is used in a non-temporal context,
 *  *           or LogicType.Temporal for when the UnaryOperator is used in a temporal context.
 */
public abstract class UnaryOperator<T extends LogicType, T2 extends LogicType> implements Predicate<T2>{
    /**
     * Constructor of UnaryOperator.
     * @param x sub-predicate
     */
    UnaryOperator(Predicate<T> x){
        subPredicate = x;
    }

    /**
     * Returns the sub-predicate.
     * @return the sub-predicate
     */
    public Predicate<T> getSubPredicate(){
        return subPredicate;
    }

    /**
     * Sets the sub-predicate.
     * @param predicate what the sub-predicate should be set to.
     */
    public void setSubPredicate(Predicate<T> predicate){
        subPredicate = predicate;
    }

    private Predicate<T> subPredicate;
}