package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * Atomic is a final predicate without any sub-predicates. It can only be used in non-temporal contexts.
 * Examples of atomics could be, "program is at line x", "size() > i".
 */
public interface Atomic extends Predicate<LogicType.NonTemporal>{}