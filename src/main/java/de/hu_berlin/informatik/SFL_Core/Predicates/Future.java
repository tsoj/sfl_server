package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * Future is a temporal operator that can be used to express that the sub-predicate must be true at some time in the future.
 * @param <T> LogicType of the sub-predicate
 */
public class Future<T extends LogicType> extends TemporalOperator<T>{
    /**
     * Constructor of Future.
     * @param x sub-predicate
     */
    public Future(Predicate<T> x) {
        super(x);
    }
}
