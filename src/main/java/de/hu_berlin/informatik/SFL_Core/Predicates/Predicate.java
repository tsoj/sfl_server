package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * Interface Predicate. Can be used to describe logical constructs.
 * @param <T> LogicType specifies which in which logical context the predicate is used
 */
public interface Predicate<T extends LogicType>{}

