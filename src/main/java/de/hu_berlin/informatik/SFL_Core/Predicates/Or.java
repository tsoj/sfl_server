package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * Or is a BinaryOperator that can be used to express that at least one of the two sub-predicates must be true.
 * @param <T> LogicType to specify in which context this predicate is used
 */
public class Or<T extends LogicType> extends BinaryOperator<T, T>{
    /**
     * Constructor of Or.
     * @param x first sub-predicate
     * @param y second sub-predicate
     */
    public Or(Predicate<T> x, Predicate<T> y) {
        super(x, y);
    }
}
