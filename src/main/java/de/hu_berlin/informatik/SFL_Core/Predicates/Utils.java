package de.hu_berlin.informatik.SFL_Core.Predicates;

import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.AtLine;
import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.BiggerThan;
import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.Equality;
import de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics.True;

import java.util.ArrayList;

/**
 * Utils provides some useful functionality for working with predicates.
 */
public class Utils {
    /**
     * Given some predicate, getPredicateList returns a list containing all predicates that are sub-predicates of this
     * predicate, sub-sub-predicates, sub-sub-…-predicates. Predicates that are not instances of type T are excluded.
     * If meeting this requirement, the top level predicate is also included in the list.
     * @param predicate predicate of which we want a list of sub-predicates
     * @param type Class of T
     * @param <T> type mask defining which predicates are included in the list
     * @return list of sub-…-predicates of the given predicate
     */
    static public <T extends Predicate<?>> ArrayList<T> getSubPredicateList(Predicate<?> predicate, Class<T> type){
        var result = new ArrayList<T>();

        if(type.isInstance(predicate)) {
            result.add((type.cast(predicate)));
        }
        if(predicate instanceof BinaryOperator<?,?>){
            result.addAll(getSubPredicateList(((BinaryOperator<?,?>) predicate).getSubPredicateA(), type));
            result.addAll(getSubPredicateList(((BinaryOperator<?,?>) predicate).getSubPredicateB(), type));
        }
        if(predicate instanceof UnaryOperator<?,?>){
            result.addAll(getSubPredicateList(((UnaryOperator<?,?>) predicate).getSubPredicate(), type));
        }

        return result;
    }

    /**
     * Given some predicate, getPredicateList returns a list containing all predicates that are sub-predicates of this
     * predicate, sub-sub-predicates, sub-sub-…-predicates.
     * The top level predicate is also included in the list.
     * @param predicate predicate of which we want a list of sub-predicates
     * @return list of sub-…-predicates of the given predicate
     */
    static public ArrayList<Predicate> getSubPredicateList(Predicate<?> predicate){
        return getSubPredicateList(predicate, Predicate.class);
    }

    /**
     * Checks whether the predicate is a temporal predicate.
     * @param predicate predicate we want to check
     * @return returns true if predicate is a temporal predicate, and false in case it is not
     */
    static public boolean isTemporal(Predicate<?> predicate){
        for(var p : getSubPredicateList(predicate))
            if(p instanceof TemporalOperator)
                return true;

        return false;
    }

    /**
     * getJavaConditionString creates a string that represents the predicate in Java syntax.
     * Only works for default Atomics and Predicates.
     * @param predicate predicate
     * @return string of the predicate in Java syntax
     */
    static public String getJavaConditionString(Predicate<?> predicate){

        if(predicate instanceof BinaryOperator<?,?>){
            var var1 = "(" + getJavaConditionString(((BinaryOperator<?,?>) predicate).getSubPredicateA()) + ")";
            var var2 = "(" + getJavaConditionString(((BinaryOperator<?,?>) predicate).getSubPredicateB()) + ")";
            var op = " <unknown operator> ";
            if(predicate instanceof And<?>){
                op = " && ";
            }
            if(predicate instanceof Or<?>){
                op = " || ";
            }
            return var1 + op + var2;
        }
        if(predicate instanceof UnaryOperator<?,?>){
            var var1 = "(" + getJavaConditionString(((UnaryOperator<?,?>) predicate).getSubPredicate()) + ")";
            var op = " <unknown operator>";
            if (predicate instanceof  Not<?>){
                op = " !";
            }
            if (predicate instanceof  Globally<?>){
                op = " Globally";
            }
            if (predicate instanceof  Future<?>){
                op = " Future";
            }
            return op + var1;
        }
        if(predicate instanceof AtLine){
            return "true";
        }
        if(predicate instanceof BiggerThan){
            return ((BiggerThan) predicate).varA + " > " + ((BiggerThan) predicate).varB;
        }
        if(predicate instanceof Equality){
            return ((Equality) predicate).varA + " == " + ((Equality) predicate).varB;
        }
        if(predicate instanceof True){
            return "true";
        }
        //TODO: include new Atomics or Predicate types

        return "";
    }
}
