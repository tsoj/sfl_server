package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * TemporalOperator is an abstract UnaryOperator that can be used as a template for predicates that are
 * temporal operators. I.e., operators that can be used in a temporal logic context and at the same time can
 * have a temporal or non-temporal sub-predicate.
 * @param <T> either LogicType.NonTemporal when the TemporalOperator has a non-temporal sub-predicate,
 *           or LogicType.Temporal for when the TemporalOperator has a temporal sub-predicate.
 */
public abstract class TemporalOperator<T extends LogicType> extends UnaryOperator<T, LogicType.Temporal>{
    /**
     * Constructor of TemporalOperator.
     * @param x sub-predicate
     */
    TemporalOperator(Predicate<T> x){
        super(x);
    }
}
