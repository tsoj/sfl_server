package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * Class that contains different types of logic.
 */
public abstract class LogicType {
    /**
     * NonTemporal can be used to describe a non-temporal logic context. For example, predicates at nodes should be
     * non-temporal.
     */
    public static abstract class NonTemporal extends LogicType {
    }

    /**
     * Temporal can be used to describe a non-temporal logic context. For example, predicates at nodes should be
     * non-temporal.
     */
    public static abstract class Temporal extends LogicType{
    }
}
