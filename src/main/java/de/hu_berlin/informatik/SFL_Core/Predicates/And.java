package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * And is a predicate that can be used to require that two sub-predicates evaluate to true.
 * @param <T> either LogicType.NonTemporal when the And is used in a non-temporal context,
 *           or LogicType.Temporal for when the And is used in a temporal context.
 */
public class And<T extends LogicType> extends BinaryOperator<T, T>{
    /**
     * Constructor of And.
     * @param x first sub-predicate
     * @param y second sub-predicate
     */
    public And(Predicate<T> x, Predicate<T> y) {
        super(x, y);
    }
}
