package de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics;

import de.hu_berlin.informatik.SFL_Core.Predicates.Atomic;

/**
 * True is an Atomic. It explains itself.
 */
public class True implements Atomic {
}
