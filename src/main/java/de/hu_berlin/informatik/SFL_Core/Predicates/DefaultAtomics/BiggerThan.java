package de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics;

import de.hu_berlin.informatik.SFL_Core.Predicates.Atomic;

/**
 * BiggerThan is an Atomic that can be used to describe the requirement of one variable to be bigger than another.
 * I.e., a > b
 */
public class BiggerThan implements Atomic{
    public String varA;
    public String varB;
    /**
     * Constructor for BiggerThan.
     * @param a string of variable name of the bigger variable
     * @param b string of the variable name of the smaller variable
     */
    public BiggerThan(String a, String b){
        varA = a;
        varB = b;
    }
}
