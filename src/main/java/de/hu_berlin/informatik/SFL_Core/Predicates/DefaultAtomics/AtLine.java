package de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics;


import de.hu_berlin.informatik.SFL_Core.Predicates.Atomic;

/**
 * AtLine is an Atomic that can be used to describe that a predicate requires that the program is at this line.
 */
public class AtLine implements Atomic {
    /**
     * AtLine constructor.
     * @param line line at which the program should be
     * @param path path to the source file to which the line corresponds
     */
    public AtLine(int line, String path) {
        this.line = line;
        this.path = path;
    }
    // TODO fix example JSON FDTSs
    public int line;
    public String path;
}
