package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * Globally is a temporal operator that can be used to express that the sub-predicate must be always true on the specified path.
 * The specified path in this context is the edge to which this predicate is connected.
 * @param <T> LogicType of the sub-predicate
 */
public class Globally<T extends LogicType> extends TemporalOperator<T> {
    /**
     * Constructor of Globally.
     * @param x sub-predicate
     */
    public Globally(Predicate<T> x) {
        super(x);
    }
}
