package de.hu_berlin.informatik.SFL_Core.Predicates.DefaultAtomics;

import de.hu_berlin.informatik.SFL_Core.Predicates.Atomic;

/**
 * Equality is an Atomic that can be used to describe the requirement that two variables are equal.
 */
public class Equality implements Atomic {
    public String varA;
    public String varB;
    /**
     * Constructor of Equality.
     * @param a variable name of one variable
     * @param b variable name of the other variable
     */
    public Equality(String a, String b){
        varA = a;
        varB = b;
    }
}
