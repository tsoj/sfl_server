package de.hu_berlin.informatik.SFL_Core.Predicates;

/**
 * Not is a UnaryOperator that can be used to express that the sub-predicate must not be true.
 * @param <T> LogicType to specify the context in which this predicate is used
 */
public class Not<T extends LogicType> extends UnaryOperator<T, T>{
    /**
     * Constructor of Not
     * @param x sub-predicate
     */
    public Not(Predicate<T> x) {
        super(x);
    }
}
