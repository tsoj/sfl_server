package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class CreateConsumerWindow {
    private JPanel panel1;
    private JList<Object> pipelineList;
    private final DefaultListModel<Object> pipelineListModel;
    private JList<NamedConsumerFactory> consumerFactoryList;
    private JList<NamedTransformerFactory> transformerFactoryList;
    private JButton addButton;
    private JButton removeButton;
    private JButton createButton;
    private JTextField nameTextField;
    private JButton cancelButton;

    private boolean movedConsumerOutside = false;

    private static class NamedConsumerFactory{//TODO reduce code duplication
        NamedConsumerFactory(ConsumerFactory consumerFactory){
            this.consumerFactory = consumerFactory;
        }
        ConsumerFactory consumerFactory;
        @Override
        public String toString(){
            return consumerFactory.createConsumer().getName();
        }
    }
    private static class NamedTransformerFactory{
        NamedTransformerFactory(TransformerFactory transformerFactory){
            this.transformerFactory = transformerFactory;
        }
        TransformerFactory transformerFactory;
        @Override
        public String toString(){
            return transformerFactory.createTransformer().getName();
        }
    }

    public CreateConsumerWindow(Runnable switchToConsumerManagerWindow, MainWindow.CreateConsumerFactory createConsumerFactory){
        var consumerFactoryListModel = new DefaultListModel<NamedConsumerFactory>();
        consumerFactoryList.setModel(consumerFactoryListModel);
        consumerFactoryList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        var transformerFactoryListModel = new DefaultListModel<NamedTransformerFactory>();
        transformerFactoryList.setModel(transformerFactoryListModel);
        transformerFactoryList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        var current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader()); // necessary (I don't know why)
            ServiceLoader<ConsumerFactory> consumerFactoryLoader = ServiceLoader.load(ConsumerFactory.class);
            for(var consumerFactory : consumerFactoryLoader) {
                consumerFactoryListModel.addElement(new NamedConsumerFactory(consumerFactory));
            }
            ServiceLoader<TransformerFactory> transformerFactoryLoader = ServiceLoader.load(TransformerFactory.class);
            for(var transformerFactory : transformerFactoryLoader) {
                transformerFactoryListModel.addElement(new NamedTransformerFactory(transformerFactory));
            }
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }

        pipelineListModel = new DefaultListModel<>();
        pipelineList.setModel(pipelineListModel);
        pipelineList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        consumerFactoryList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if(!consumerFactoryList.isSelectionEmpty())
                    transformerFactoryList.clearSelection();
            }
        });
        transformerFactoryList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if(!transformerFactoryList.isSelectionEmpty())
                    consumerFactoryList.clearSelection();
            }
        });

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                assert consumerFactoryList.isSelectionEmpty() || transformerFactoryList.isSelectionEmpty();

                if(pipelineListModel.isEmpty() || (pipelineListModel.lastElement() instanceof NamedTransformerFactory)){
                    if(!consumerFactoryList.isSelectionEmpty()){
                        var selectedList = consumerFactoryList.getSelectedValuesList();
                        assert selectedList.size() == 1;
                        pipelineListModel.addElement(selectedList.get(0));
                    }
                    if(!transformerFactoryList.isSelectionEmpty()){
                        var selectedList = transformerFactoryList.getSelectedValuesList();
                        assert selectedList.size() == 1;
                        pipelineListModel.addElement(selectedList.get(0));
                    }
                }
            }
        });

        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(!pipelineList.isSelectionEmpty()){
                    pipelineListModel.remove(pipelineList.getSelectedIndex());
                }
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switchToConsumerManagerWindow.run();
            }
        });

        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                assert !movedConsumerOutside;
                var current = getCurrentConsumerFactory();
                if(current == null)
                    return;
                createConsumerFactory.run(current);
                movedConsumerOutside = true;
                switchToConsumerManagerWindow.run();
                // TODO: this class can only be used ONCE to create a custom consumer. fix that or make that clear somehow. IMPORTANT
            }
        });

        // TODO: add other buttons


    }


    public JComponent getContent() {
        return panel1;
    }

    public ConsumerFactory getCurrentConsumerFactory(){
        if (pipelineListModel.isEmpty() || !(pipelineListModel.lastElement() instanceof NamedConsumerFactory))
            return null;
        return new ConsumerFactory(){
            final ArrayList<TransformerFactory> transformerFactoryList = new Object(){
                ArrayList<TransformerFactory> run(){
                    var result = new ArrayList<TransformerFactory>();
                    for(int i = 0; i<pipelineListModel.size() - 1; ++i){
                        var transformerFactory = pipelineListModel.get(i);
                        assert transformerFactory instanceof NamedTransformerFactory;
                        result.add(((NamedTransformerFactory) transformerFactory).transformerFactory);
                    }
                    return result;
                }
            }.run();
            final ConsumerFactory consumerFactory = ((NamedConsumerFactory) pipelineListModel.lastElement()).consumerFactory;

            @Override
            public void settings(@NotNull ToolWindow toolWindow, @NotNull Project project) {
                var combinedConsumerSettingsWindow = new CombinedConsumerSettingsWindow(
                        toolWindow,
                        project,
                        transformerFactoryList,
                        consumerFactory
                );
                ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
                Content content = contentFactory.createContent(combinedConsumerSettingsWindow.getContent(), createConsumer().getName() + " settings", false);
                toolWindow.getContentManager().addContent(content);
                content.setCloseable(true);
            }

            @Override
            public Consumer createConsumer() {

                return new Consumer() {

                    final ArrayList<Transformer> transformerList = new Object(){
                        ArrayList<Transformer> run(){
                            var result = new ArrayList<Transformer>();
                            for(var factory : transformerFactoryList)
                                result.add(factory.createTransformer());
                            return result;
                        }
                    }.run();
                    final Consumer consumer = consumerFactory.createConsumer();

                    @Override
                    public void run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList) {
                        var currentFDTSList = fdtsList;
                        for(var transformer : transformerList){
                            currentFDTSList = transformer.run(toolWindow, project, currentFDTSList);
                        }
                        consumer.run(toolWindow, project, currentFDTSList);
                    }

                    @Override
                    public String getName() {
                        return nameTextField.getText().isEmpty() ? "untitled consumer" : nameTextField.getText();
                    }

                    @Override
                    public void setActiveElement(@NotNull FDTS fdts, FDTS.@NotNull ID id) {
                        consumer.setActiveElement(fdts, id);
                    }

                    @Override
                    public void setActiveElementCallback(@NotNull ActiveElementChangeCallback callback) {
                        consumer.setActiveElementCallback(callback);
                    }
                };
            }
        };
    }
}
