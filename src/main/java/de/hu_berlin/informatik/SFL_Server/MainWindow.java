package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.uiDesigner.core.GridConstraints;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.TreeMap;

// TODO move all windows stuff to extra package
// TODO maybe save configuration for next project session

public class MainWindow {
    private JPanel panel1;
    private JTabbedPane tabbedPaneFDTSLists;
    private JButton addTabButton;
    private JButton closeTabButton;
    private JPanel consumerPanel;

    private ConsumerManagerWindow consumerManagerWindow;
    // TODO: be consistent with naimg scheme of lists (e.g. components vs componentList)
    private final ArrayList<FDTSListWindow> fdtsListWindows;

    public interface CreateConsumerFactory {
        void run(ConsumerFactory consumerFactory);
    }

    public MainWindow(ToolWindow toolWindow, Project project){
        //TODO: handle tabbed stuff.
        fdtsListWindows = new ArrayList<>();

        tabbedPaneFDTSLists.removeAll();

        consumerManagerWindow = new ConsumerManagerWindow(
                toolWindow,
                project,
                () -> {//GetSelectedFDTS
                    for(var window : fdtsListWindows){
                        if(window.getContent() == tabbedPaneFDTSLists.getSelectedComponent()){
                            return window.getSelectedFDTSList();
                        }
                    }
                    return null;
                },
                () -> {
                    var createConsumerWindow = new CreateConsumerWindow(
                            () -> {
                                if (consumerManagerWindow != null) {
                                    consumerPanel.removeAll();
                                    var content = consumerManagerWindow.getContent();
                                    assert content != null;
                                    consumerPanel.add(content, new GridConstraints());

                                }
                            },
                            new CreateConsumerFactory() {
                                @Override
                                public void run(ConsumerFactory consumerFactory) {
                                    consumerManagerWindow.addConsumerFactory(consumerFactory);
                                }
                            }
                    );
                    consumerPanel.removeAll();
                    var content = createConsumerWindow.getContent();
                    assert content != null;
                    consumerPanel.add(content, new GridConstraints());
                }
        );
        consumerPanel.add(consumerManagerWindow.getContent(), new GridConstraints());

        addTabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                tabCounter += 1;
                var newWindow = new FDTSListWindow(project);
                fdtsListWindows.add(newWindow);
                tabbedPaneFDTSLists.addTab("untitled " + tabCounter, newWindow.getContent()); // TODO name tabs properly (how?)
            }
        });
        closeTabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(tabbedPaneFDTSLists.getTabCount() == 0)
                    return;
                var currentIndex = tabbedPaneFDTSLists.getSelectedIndex();
                var oldSize = fdtsListWindows.size();
                fdtsListWindows.removeIf(window -> window.getContent() == tabbedPaneFDTSLists.getComponentAt(currentIndex));
                assert oldSize == fdtsListWindows.size() + 1;
                tabbedPaneFDTSLists.removeTabAt(currentIndex);
            }
        });
    }
    private int tabCounter = 0;

    public JComponent getContent() {
        return panel1;
    }

}
