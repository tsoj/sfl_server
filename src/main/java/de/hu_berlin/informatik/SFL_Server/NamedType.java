package de.hu_berlin.informatik.SFL_Server;

/**
 * An interface for giving a displayable, readable name to a object type.
 */
public interface NamedType {
    /**
     * This should return a static string that is unique to the implementing type.
     * @return unique name string
     */
    String getName();
}
