package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import org.jetbrains.annotations.NotNull;
//import org.jgrapht.ListenableGraph;
//import org.jgrapht.graph.DefaultDirectedGraph;
//import org.jgrapht.graph.DefaultEdge;
//import org.jgrapht.graph.DefaultListenableGraph;
//import org.jgrapht.*;
//import org.jgrapht.ext.*;
//import org.jgrapht.graph.*;
//import org.graphstream.ui.swing.*;
//import org.graphstream.ui.view.*;

import java.util.List;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.ServiceLoader;

public class ConsumerManagerWindow {
    private JList<NamedConsumerFactory> consumerFactoryList;//TODO again switch to consumer factory
    private JPanel panel1;
    private JButton runConsumerButton;
    private JButton createConsumerTypeButton;
    private JButton consumerSettingsButton;
    private JButton removeButton;

    private final DefaultListModel<NamedConsumerFactory> consumerFactoryListModel;

    public interface GetSelectedFDTS{
        // TODO: maybe return list for future combining modules
        public List<FDTS> get();
    }

    private static class NamedConsumerFactory {
        NamedConsumerFactory(ConsumerFactory consumerFactory){
            this.consumerFactory = consumerFactory;
        }
        ConsumerFactory consumerFactory;
        @Override
        public String toString(){
            return consumerFactory.createConsumer().getName();
        }
    }

    public ConsumerManagerWindow(
            ToolWindow toolWindow,
            Project project,
            GetSelectedFDTS getSelectedFDTS,
            Runnable switchToCreateConsumerWindow
    ){


        consumerFactoryListModel = new DefaultListModel<>();
        consumerFactoryList.setModel(consumerFactoryListModel);
        // TODO: show name instead of generic toString()
        consumerFactoryList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        ClassLoader current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
            ServiceLoader<ConsumerFactory> consumerFactoryLoader = ServiceLoader.load(ConsumerFactory.class);
            for(var consumerFactory : consumerFactoryLoader) {
                consumerFactoryListModel.addElement(new NamedConsumerFactory(consumerFactory));
            }
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }

        // TODO create consumer types from pure consumer provided by extensions

        runConsumerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                var fdtsList = getSelectedFDTS.get();
                if(fdtsList == null)
                    return;

                var connectableList = new ArrayList<Connectable>();
                var consumerList = new ArrayList<Consumer>();

                for(var consumerFactory : ConsumerManagerWindow.this.consumerFactoryList.getSelectedValuesList()){
                    var consumer = consumerFactory.consumerFactory.createConsumer();
                    consumerList.add(consumer);
                    connectableList.add(consumer);
                }

                for(var connectable : connectableList){
                    // TODO: test (check for memory leaks?)
                    connectable.setActiveElementCallback(new Connectable.ActiveElementChangeCallback() {
                        private final ArrayList<Connectable> list = connectableList;
                        @Override
                        public void onChange(@NotNull FDTS fdts, FDTS.@NotNull ID id) {
                            for(var otherConnectable : this.list)
                            {
                                if(otherConnectable != connectable){
                                    otherConnectable.setActiveElement(fdts, id);
                                }
                            }
                        }
                    });
                }
                for(var consumer : consumerList){
                    consumer.run(toolWindow, project, fdtsList);
                }
            }
        });

        createConsumerTypeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switchToCreateConsumerWindow.run();
            }
        });
        // TODO: otherbuttons (settings)
        consumerSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                var selected = consumerFactoryList.getSelectedValuesList();
                assert selected != null;
                for(var consumerFactory : selected){
                    if(consumerFactory.consumerFactory != null)
                        ((Settable) consumerFactory.consumerFactory).settings(toolWindow, project);
                }

            }
        });

        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                var selected = consumerFactoryList.getSelectedValuesList();
                assert selected != null;
                for(var o : selected){
                    consumerFactoryListModel.removeElement(o);
                }

            }
        });
    }

    public Component getContent() {
        return panel1;
    }

    public void addConsumerFactory(ConsumerFactory consumerFactory){
        consumerFactoryListModel.addElement(new NamedConsumerFactory(consumerFactory));
    }
}
