package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import org.jetbrains.annotations.NotNull;

/**
 * Settable is an interface to provide the option of change the settings of a type. In this context this means
 * type factories. The settings should then be considered when creating a new type instance.
 */
public interface Settable {
    /**
     * Will be called from the outside when the user selects this type (factory) to edit settings. This then should
     * provide the user with an interface using the toolWindow and IDE project to allow changing relevant settings.
     * @param toolWindow Intellij tool window that can be used to add windows as tabs to expose the user to functionality
     * @param project IDE project that can be used to access different parts of the IDE environment
     */
    default void settings(@NotNull ToolWindow toolWindow, @NotNull Project project){}
}
