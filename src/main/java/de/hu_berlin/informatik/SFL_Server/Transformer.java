package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import org.jetbrains.annotations.NotNull;

import java.util.List;
/**
 * Transformer is an interface that can be implemented by extensions that want to provide a service to the user for a given
 * list of FDTS objects, that also create a new list of FDTSs by running.
 * In the user interface this appears as the possibility to create a new consumer type by
 * putting a number of transformers behind each other and at the end some consumer.
 */
public interface Transformer extends NamedType{
    /**
     * This will be called from the outside when this transformer is selected to be executed for a given list of FDTS objects.
     * @param toolWindow Intellij tool window that can be used to add windows as tabs to expose the user to functionality
     * @param project IDE project that can be used to access different parts of the IDE environment
     * @param fdtsList list of FDTS object that the transformer should run on
     * @return new list of FDTS objects
     */
    @NotNull List<FDTS> run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList);
}
