package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Consumer is an interface that can be implemented by extensions that want to provide a service to the user for a given
 * list of FDTS objects.
 */
public interface Consumer extends NamedType, Connectable{
    /**
     * This will be called from the outside when this consumer is selected to be executed for a given list of FDTS objects.
     * @param toolWindow Intellij tool window that can be used to add windows as tabs to expose the user to functionality
     * @param project IDE project that can be used to access different parts of the IDE environment
     * @param fdtsList list of FDTS object that the consumer should run on
     */
    void run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList);
}
