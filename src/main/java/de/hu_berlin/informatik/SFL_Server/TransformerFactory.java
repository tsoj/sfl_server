package de.hu_berlin.informatik.SFL_Server;
/**
 * TransformerFactory is an interface that must be implemented to provide the user with the possibility to select
 * a custom Transformer.
 */
public interface TransformerFactory extends Settable{
    /**
     * Returns a new transformer. The Transformer instance shouldn't be dependent on the state of this TransformerFactory.
     * @return transformer conforming to the settings the user selected
     */
    Transformer createTransformer();
}
