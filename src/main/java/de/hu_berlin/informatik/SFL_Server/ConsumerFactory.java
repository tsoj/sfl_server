package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import org.jetbrains.annotations.NotNull;

/**
 * ConsumerFactory is an interface that must be implemented to provide the user with the possibility to select
 * a custom Consumer.
 */
public interface ConsumerFactory extends Settable{
    /**
     * Returns a new consumer. The Consumer instance shouldn't be dependent on the state of this ConsumerFactory.
     * @return consumer conforming to the settings the user selected
     */
    Consumer createConsumer();
}
