package de.hu_berlin.informatik.SFL_Server;

import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import org.jetbrains.annotations.NotNull;

/**
 * Connectable is an interface that provides an option to allow sending message to simultaneously executed
 * FDTS consumer. Currently the only message that can be sent is which element of an FDTS (edge or node) is selected.
 * In future there might be more options.
 */
// TODO allow arbitrary messages
public interface Connectable {
    interface ActiveElementChangeCallback{
        /**
         * This should be called by a consumer when the internal state changes such that a new element is selected.
         * (What "selected" means will depend on the particular task that the consumer does.)
         * @param fdts selected FDTS
         * @param id selected FDTS element (node or edge)
         */
        void onChange(@NotNull FDTS fdts, @NotNull FDTS.ID id);
    }

    /**
     * This will be called from the outside of the consumer when a connected consumer reported a change in its selection.
     * @param fdts selected FDTS
     * @param id selected element
     */
    default void setActiveElement(@NotNull FDTS fdts, @NotNull FDTS.ID id){};

    /**
     * This will be called from the outside of the consumer to set the ActiveElementChangeCallback that the consumer
     * should use when it detects a relevant change in its internal state.
     * @param callback ActiveElementChangeCallback
     */
    default void setActiveElementCallback(@NotNull ActiveElementChangeCallback callback){};
}
