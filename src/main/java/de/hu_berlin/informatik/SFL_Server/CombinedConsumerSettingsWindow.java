package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CombinedConsumerSettingsWindow {
    private JList<NamedSettable> pipelineList;
    private JPanel panel1;
    private JButton settingsButton;

    private static class NamedSettable{//TODO reduce code duplication
        NamedSettable(Settable settable, String name){
            this.name = name;
            this.settable = settable;
        }
        Settable settable;
        String name;
        @Override
        public String toString(){
            return name;
        }
    }

    CombinedConsumerSettingsWindow(
            @NotNull ToolWindow toolWindow, Project project,
            List<TransformerFactory> transformerFactoryList, ConsumerFactory consumerFactory
    ){

        DefaultListModel<NamedSettable> pipelineListModel = new DefaultListModel<>();
        pipelineList.setModel(pipelineListModel);
        pipelineList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        for(var transformerFactory : transformerFactoryList)
            pipelineListModel.addElement(new NamedSettable(transformerFactory, transformerFactory.createTransformer().getName()));
        pipelineListModel.addElement(new NamedSettable(consumerFactory, consumerFactory.createConsumer().getName()));

        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                var selected = pipelineList.getSelectedValue();
                if(selected == null)
                    return;
                selected.settable.settings(toolWindow, project);
            }
            //TODO: test
        });
    }

    public JComponent getContent() {
        return panel1;
    }
}
