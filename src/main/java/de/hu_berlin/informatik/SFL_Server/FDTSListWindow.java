package de.hu_berlin.informatik.SFL_Server;

import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.fileEditor.impl.LoadTextUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTSLoader;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTSMeta;
import de.hu_berlin.informatik.SFL_Core.Predicates.Predicate;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ServiceLoader;

public class FDTSListWindow {

    static String filenameKey = "filename";
    static String susKey = "sus";
    static String indexKey = "index";

    private static class NamedFDTS{
        NamedFDTS(FDTSMeta fdts){
            this.fdts = fdts;
        }
        FDTSMeta fdts;
        @Override
        public String toString(){
            var filename = fdts.info.get(filenameKey);
            var index = fdts.info.get(indexKey);
            var sus = fdts.info.get(susKey);
            var result = filename != null ? filename : "untitled";
            if(index != null)
                result += ":" + index;
            if(sus != null)
                result += ":" + sus;
            return result;
        }
    }

    private JList<NamedFDTS> fdtsList;
    private JPanel panel1;
    private JButton addFDTSButton;
    private JButton removeFDTSButton;
    private JComboBox comboBox1;// TODO: implement sorting

    public FDTSListWindow(Project project){

        var current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(FDTSLoader.class.getClassLoader());
            var loader = ServiceLoader.load(Predicate.class);
            for(var predicate : loader) {
                FDTSLoader.registerPredicate(predicate.getClass());
            }
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }

        var fdstListModel = new DefaultListModel<NamedFDTS>();
        fdtsList.setModel(fdstListModel);

        addFDTSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                var fileChooserDescriptor = new FileChooserDescriptor(
                        true,
                        false,
                        false,
                        false,
                        false,
                        false
                );
                fileChooserDescriptor.setTitle("Add FDTS");
                FileChooser.chooseFile(fileChooserDescriptor, project, null, new com.intellij.util.Consumer<VirtualFile>() {
                    @Override
                    public void consume(VirtualFile virtualFile) {
                        var text = LoadTextUtil.loadText(virtualFile).toString();
                        try {
                            var fdts = FDTSLoader.loadFromJson(text, FDTSMeta.class);
                            fdts.info.put(filenameKey, virtualFile.getPresentableUrl());
                            fdstListModel.addElement(new NamedFDTS(fdts));
                        } catch (Throwable e) {
                            try {
                                var fdtsList = FDTSLoader.loadMetaArrayFromJson(text);
                                for(var fdts : fdtsList){
                                    fdts.info.put(filenameKey, virtualFile.getPresentableUrl());
                                    fdts.info.put(indexKey, "" + fdtsList.indexOf(fdts));
                                    //TODO: index shouldn't be part of the filename
                                    fdstListModel.addElement(new NamedFDTS(fdts));
                                }
                            } catch (Throwable f) {
                                System.out.println(e.toString());
                                System.out.println(f.toString());
                            }
                        }
                    }
                }); // TODO: allow reading array of fdts
            }
        });

        removeFDTSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(!fdtsList.isSelectionEmpty())
                    fdstListModel.remove(fdtsList.getSelectedIndex());
            }
        });
    }

    public Component getContent() {
        return panel1;
    }

    public List<FDTS> getSelectedFDTSList(){
        var result = new ArrayList<FDTS>();
        for(var namedFDTS : fdtsList.getSelectedValuesList())
            result.add(namedFDTS.fdts);
        return result;
    }
}
