package de.hu_berlin.informatik.SFL_Server_Extension_Print_Json_FDTS;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTSLoader;
import de.hu_berlin.informatik.SFL_Server.Connectable;
import de.hu_berlin.informatik.SFL_Server.Consumer;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PrintJsonFDTS implements Consumer {
    private ActiveElementChangeCallback callback;

    @Override
    public void setActiveElement(@NotNull FDTS fdts, @NotNull FDTS.ID id) {

    }

    @Override
    public void setActiveElementCallback(@NotNull ActiveElementChangeCallback callback) {
        this.callback = callback;
    }

    @Override
    public void run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList) {

        assert !fdtsList.isEmpty();// TODO proper error handling

        PrintJsonFDTSWindow printJsonFDTSWindow = new PrintJsonFDTSWindow(toolWindow);
        printJsonFDTSWindow.setText(FDTSLoader.getJson(fdtsList));
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        Content content = contentFactory.createContent(printJsonFDTSWindow.getContent(), getName(), false);
        toolWindow.getContentManager().addContent(content);
        content.setCloseable(true);
        // TODO: add new content, fill in description,
        //  listen for mouseclicks->ActiveElementChangeCallback callback, etc
    }

    @Override
    public String getName() {
        return "Print Json FDTS";
    }
}
