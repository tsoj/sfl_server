package de.hu_berlin.informatik.SFL_Server_Extension_Print_Json_FDTS;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import de.hu_berlin.informatik.SFL_Server.Consumer;
import de.hu_berlin.informatik.SFL_Server.ConsumerFactory;
import org.jetbrains.annotations.NotNull;

public class PrintJsonFDTSFactory implements ConsumerFactory {
    @Override
    public Consumer createConsumer() {
        return new PrintJsonFDTS();
    }

    @Override
    public void settings(@NotNull ToolWindow toolWindow, @NotNull Project project) {

    }
}
