package de.hu_berlin.informatik.SFL_Server_Extension_Print_Json_FDTS;

import com.intellij.openapi.wm.ToolWindow;

import javax.swing.*;

public class PrintJsonFDTSWindow {
    private JPanel panel1;
    private JTextArea textArea1;

    public PrintJsonFDTSWindow(ToolWindow toolWindow) {
    }

    public JComponent getContent() {
        return panel1;
    }

    public void setText(String text){
        textArea1.setText(text);
    }
}
