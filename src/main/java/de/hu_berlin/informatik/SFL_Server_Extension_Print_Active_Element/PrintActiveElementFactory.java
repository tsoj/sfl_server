package de.hu_berlin.informatik.SFL_Server_Extension_Print_Active_Element;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import de.hu_berlin.informatik.SFL_Server.Consumer;
import de.hu_berlin.informatik.SFL_Server.ConsumerFactory;
import de.hu_berlin.informatik.SFL_Server_Extension_Print_FDTS_Graph.PrintFDTSGraph;
import org.jetbrains.annotations.NotNull;

public class PrintActiveElementFactory implements ConsumerFactory {
    @Override
    public Consumer createConsumer() {
        return new PrintActiveElement();
    }

    @Override
    public void settings(@NotNull ToolWindow toolWindow, @NotNull Project project) {

    }
}
