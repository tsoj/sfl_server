package de.hu_berlin.informatik.SFL_Server_Extension_Print_Active_Element;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTSLoader;
import de.hu_berlin.informatik.SFL_Server.Connectable;
import de.hu_berlin.informatik.SFL_Server.Consumer;
import de.hu_berlin.informatik.SFL_Server_Extension_Print_Json_FDTS.PrintJsonFDTSWindow;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PrintActiveElement implements Consumer {
    private final PrintActiveElementWindow printActiveElementWindow = new PrintActiveElementWindow();

    @Override
    public void setActiveElement(@NotNull FDTS fdts, @NotNull FDTS.ID id) {
        printActiveElementWindow.setText(
                FDTSLoader.getJson(id) + ":\n" +
                FDTSLoader.getJson(fdts.getPredicate(id))
        );
    }

    @Override
    public void setActiveElementCallback(@NotNull ActiveElementChangeCallback callback) {

    }

    @Override
    public void run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList) {
        printActiveElementWindow.setText(FDTSLoader.getJson(fdtsList));
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        Content content = contentFactory.createContent(printActiveElementWindow.getContent(), getName(), false);
        toolWindow.getContentManager().addContent(content);
        content.setCloseable(true);
    }

    @Override
    public String getName() {
        return "Print Active Element";
    }
}
