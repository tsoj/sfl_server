package de.hu_berlin.informatik.SFL_Server_Extension_Print_Active_Element;

import javax.swing.*;

public class PrintActiveElementWindow {
    private JTextArea textArea1;
    private JPanel panel1;

    public JComponent getContent() {
        return panel1;
    }

    public void setText(String text){
        textArea1.setText(text);
    }
}
