package de.hu_berlin.informatik.SFL_Server_Extension_Copy_Transformer;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTSImpl;
import de.hu_berlin.informatik.SFL_Server.Transformer;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class CopyTransformer implements Transformer {

    private final int numOutgoingCopies;

    CopyTransformer(int i){
        numOutgoingCopies = i;
    }

    @Override
    public @NotNull List<FDTS> run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList) {
        var result = new ArrayList<FDTS>();
        for(var i = 0; i < numOutgoingCopies; i++)
            for(var fdts : fdtsList){
                var newFdts = new FDTSImpl();
                newFdts.transformFrom(fdts);
                result.add(fdts);
            }
        System.out.println("Num in: " + fdtsList.size() + ", num out: " + result.size());
        return result;
    }

    @Override
    public String getName() {
        return "Copy Transformer";
    }
}
