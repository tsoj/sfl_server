package de.hu_berlin.informatik.SFL_Server_Extension_Copy_Transformer;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class CopyTransformerWindow {
    private JSpinner spinner1;
    private JPanel panel1;

    static public interface SetNumber{
        void run(int i);
    }

    public CopyTransformerWindow(SetNumber setNumber){
        spinner1.setModel(new SpinnerNumberModel());
        spinner1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                assert spinner1.getValue() instanceof Integer;
                setNumber.run((Integer) spinner1.getValue());
            }
        });
    }

    public JComponent getContent() {
        return panel1;
    }
}
