package de.hu_berlin.informatik.SFL_Server_Extension_Copy_Transformer;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import de.hu_berlin.informatik.SFL_Server.Transformer;
import de.hu_berlin.informatik.SFL_Server.TransformerFactory;
import org.jetbrains.annotations.NotNull;

public class CopyTransformerFactory implements TransformerFactory {

    private int numOutgoingCopies = 1;


    @Override
    public Transformer createTransformer() {
        return new CopyTransformer(numOutgoingCopies);
    }

    @Override
    public void settings(@NotNull ToolWindow toolWindow, @NotNull Project project) {
        var trivialTransformerWithSettingsWindow = new CopyTransformerWindow(new CopyTransformerWindow.SetNumber() {
            @Override
            public void run(int i) {
                numOutgoingCopies = i;
                if(numOutgoingCopies < 1)
                    numOutgoingCopies = 1;
            }
        });
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        Content content = contentFactory.createContent(
                trivialTransformerWithSettingsWindow.getContent(),
                createTransformer().getName() + " settings",
                false
        );
        toolWindow.getContentManager().addContent(content);
        content.setCloseable(true);
    }
}
