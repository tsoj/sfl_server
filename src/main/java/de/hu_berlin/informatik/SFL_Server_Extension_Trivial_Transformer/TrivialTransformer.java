package de.hu_berlin.informatik.SFL_Server_Extension_Trivial_Transformer;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import de.hu_berlin.informatik.SFL_Core.FDTS.FDTS;
import de.hu_berlin.informatik.SFL_Server.Transformer;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TrivialTransformer implements Transformer {
    @Override
    public @NotNull List<FDTS> run(@NotNull ToolWindow toolWindow, @NotNull Project project, @NotNull List<FDTS> fdtsList) {
        return fdtsList;
    }

    @Override
    public String getName() {
        return "Trivial Transformer";
    }
}
