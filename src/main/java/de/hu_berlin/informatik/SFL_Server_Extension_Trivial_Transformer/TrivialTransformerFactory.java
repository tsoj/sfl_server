package de.hu_berlin.informatik.SFL_Server_Extension_Trivial_Transformer;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import de.hu_berlin.informatik.SFL_Server.Transformer;
import de.hu_berlin.informatik.SFL_Server.TransformerFactory;
import org.jetbrains.annotations.NotNull;

public class TrivialTransformerFactory implements TransformerFactory {
    @Override
    public Transformer createTransformer() {
        return new TrivialTransformer();
    }

    @Override
    public void settings(@NotNull ToolWindow toolWindow, @NotNull Project project) {

    }
}
